# -*- coding: utf-8 -*-  
""" 
@desc: Flask extension for focus
@author: Yetship 
@contact: liqianglau@outlook.com
@site: https://liuliqiang.info
"""
import sys

from flask import Flask

from flask_ext import escort


def main(args):
    app = Flask()

    # init focus with flask configs
    escort.init_app(app)

    app.run()


if __name__ == "__main__":
    main(sys.argv)
