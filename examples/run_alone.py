# -*- coding: utf-8 -*-
"""
@desc:
@author: Yetship
@contact: liqianglau@outlook.com
@site: https://liuliqiang.info
"""
from excort import escort as app


@app.task()
def add(x, y):
    return x + y


@app.schedule("* * * * *")
def print_helloworld():
    print("hello world!")


if __name__ == "__main__":
    app.run()
