# -*- coding: utf-8 -*-
"""
@desc: A python package implement above celery, easy to use and easy to integrate.
@author: Yetship
@contact: liqianglau@outlook.com
@site: https://liuliqiang.info
"""
import logging
from multiprocessing import Process

from celery import Celery


class WorkerProcessor(Process):
    def __init__(self, app):
        super(WorkerProcessor, self).__init__()
        self.app = app

    def run(self):
        logging.info("I am ready to execute worker")
        self.app.app.worker_main()


class BeaterProcessor(Process):
    def __init__(self, app):
        super(BeaterProcessor, self).__init__()
        self.app = app

    def run(self):
        logging.info("I am ready to execute beater")
        self.app.app.Beat().run()


class Escort(object):
    def __init__(self, broker_url='redis://localhost:6379/6'):
        self.app = Celery()
        self.app.conf.broker_url = broker_url
        self.worker_process = WorkerProcessor(self)
        self.beater_process = BeaterProcessor(self)

    def task(self):
        return self.app.task

    def schedule(self, crontab_str):
        def decorator(func):
            pass
        return decorator

    def run(self):
        self.worker_process.start()
        self.beater_process.start()

        self.worker_process.join()
        self.beater_process.join()


escort = Escort()
